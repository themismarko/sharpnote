# SharpNote
A simple tool to grab users' OneNote content.

It is common practise for users/administrators to store passwords in onenote. 

Onenote does not use local files like Excel/Word however it stores it's conten on onedrive or sharepoint. So a simple tool to make your life easier and save time from manual enumeration

## Usage
SharpNote.exe --help

```
SharpNote.exe --help
SharpNote 1.0.0.0
USAGE:
Dump NoteBooks:
  SharpNote --dump MyNote

  -e, --enum    (Default: false) List Notebooks with their sections

  -d, --dump    Provide Notebook's name to dump, "all" to dump all

  --help        Display this help screen.

  --version     Display version information.
  ```
