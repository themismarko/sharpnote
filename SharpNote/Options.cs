using System;
using System.Collections.Generic;
using System.Reflection;

namespace SharpNote
{
	internal class Options
	{
		/*
		 * any variables declared as `internal static`
		 * will be automatically handled as arguments
		 * using the format /argument:value or /switch
		 */
		internal static bool Help = false;
		internal static bool Enumerate = false;
		internal static string Dump = null;

		internal static bool ParseArguments(string[] args)
		{
			var Fields = typeof(Options).GetFields(BindingFlags.Static | BindingFlags.NonPublic);
			var ParsedArguments = new HashSet<string>();

			foreach (string arg in args)
			{
				var Argument = arg.Split(new[] { ':' }, 2);
				Argument[0] = Argument[0].Trim('/').ToLower();
				var Unknown = true;

				foreach (FieldInfo Field in Fields)
				{
					if (Argument[0] == Field.Name.ToLower())
					{
						if (ParsedArguments.Add(Argument[0]))
						{
							// string arguments
							try
							{
								Field.SetValue(null, Argument[1]);
							}

							// switches (bool)
							catch (IndexOutOfRangeException)
							{
								try
								{
									Field.SetValue(null, true);
								}
								catch (ArgumentException)
								{
									Console.WriteLine($"[-] No value specified for '{Argument[0]}'.");
									return false;
								}
							}

							// int arguments
							catch (ArgumentException)
							{
								try
								{
									Field.SetValue(null, int.Parse(Argument[1]));
								}
								catch (FormatException)
								{
									Console.WriteLine($"[-] Invalid value specified for '{Argument[0]}'.");
									return false;
								}
							}

							Unknown = false;
						}
						else
						{
							Console.WriteLine($"[-] '{Argument[0]}' argument can only be specified once.");
							return false;
						}
					}
				}

				if (Unknown)
				{
					Console.WriteLine($"[-] Unknown argument '{Argument[0]}'.");
					return false;
				}
			}

			return true;
		}
	}
}
