﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Microsoft.Office.Interop.OneNote;


namespace SharpNote
{
    class Program
    {
        private Microsoft.Office.Interop.OneNote.Application onenoteApp;
        private XNamespace ns;
        private Notebook[] notebooks;

              

        public Program()
        {
            onenoteApp = new Microsoft.Office.Interop.OneNote.Application();
            ns = null;
            notebooks = null; 
        }

        private void ShowHelp()
        {
            var Help = "";
            Help += "Usage: SharpNote.exe [options]\r\n";
            Help += "    /enumerate     :  List all Notebooks and Sections\r\n";
            Help += "    /dump          :  Dump notebook, 'all' to dump all\r\n";
            Help += "    /help          :  show help message\r\n";
            Help += "\r\nExample:\r\n";
            Help += "    SharpNote.exe /dump:all";
            Console.WriteLine(Help);
        }



        public T XMLDeserialize<T>(string input)
        {
            return (T)new XmlSerializer(typeof(T)).Deserialize(new XmlTextReader(new StringReader(input)));
        }

        public string GetHierarchy(string root, HierarchyScope hsScope)
        {
            string output;
            onenoteApp.GetHierarchy(root, hsScope, out output);
            return output;
        }

        public Notebooks GetNotebooks()
        {
            // XXX: Figure out how to deal with performance implictions of this.
            // Maybe make this a paramater.
            return this.XMLDeserialize<Notebooks>(GetHierarchy(null, HierarchyScope.hsNotebooks));
        }

        public IEnumerable<Section> GetSections(Notebook notebook, bool includePages)
        {
            return XMLDeserialize<Notebook>(GetHierarchy(notebook.ID, includePages ? HierarchyScope.hsPages : HierarchyScope.hsSections)).Section;
        }

        public IEnumerable<Page> GetPages(Section section)
        {
            return XMLDeserialize<Section>(GetHierarchy(section.ID, HierarchyScope.hsPages)).Page;
        }


        public XDocument GetPageContentAsXDocument(Page p)
        {
            return GetPageContentAsXDocument(p.ID);
        }

        public XDocument GetPageContentAsXDocument(string pageId)
        {
            string pageContent;
            onenoteApp.GetPageContent(pageId, out pageContent);
            return XDocument.Parse(pageContent);
        }


        public void printNotebooks(String name="")
        {
            bool flag = false;
            if (name != "")
                flag = true;
            for (int i = 0; i < notebooks.Length; i++)
            {
                Console.WriteLine("Notebook: "+notebooks[i].nickname);
                if (flag)
                {
                    if (name=="all" || notebooks[i].name == name)
                        printSections(notebooks[i], flag);
                }
                else
                    printSections(notebooks[i], flag);


            }
        }

         public void printSections(Notebook tmp, bool flag)
         {
            foreach (Section var in GetSections(tmp, true))
            {
                Console.WriteLine("\tSection: " + var.name);
                if (flag)
                    printPages(var);
            }
         }

        public void printPages(Section var)
        {
            Console.WriteLine("\tContent: ");
            foreach (Page var2 in GetPages(var))
            {
                var output = GetPageContentAsXDocument(var2);
                ns = output.Root.Name.Namespace;
                //Console.WriteLine("\t\t\t" + output);
                foreach (var TNode in from node in output.Descendants(ns + "T") select node)
                {
                    Console.WriteLine("\t\t" + TNode.Value);
                }
            }
        }



        static void Main(string[] args)
        {
            try
            {
                Program stil = new Program();
                stil.notebooks = stil.GetNotebooks().Notebook;


                if (Options.ParseArguments(args))
                {
                    if (Options.Help)
                    {
                        stil.ShowHelp();
                    }
                    else
                    {
                        if (Options.Enumerate)
                        {
                            Console.WriteLine("Num of Notebooks: " + stil.notebooks.Length);
                            stil.printNotebooks();
                        }
                        else
                        {
                            stil.printNotebooks(Options.Dump);
                        }

                    }
                }

                Options.Help = false;
                Options.Enumerate = false;
                Options.Dump = null;


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


        }

       

    }
}